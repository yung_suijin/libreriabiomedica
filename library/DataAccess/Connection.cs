﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace library.DataAccess
{
    public class Connection
    {
        private SqlConnection databaseLink { get; set; }

        public Connection(string connectionString)
        {
            databaseLink = new SqlConnection(connectionString);
        }

        public bool OpenConnection()
        {
            try
            {
                databaseLink.Open();
                return databaseLink.State == System.Data.ConnectionState.Open;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        public bool CloseConnection()
        {
            try
            {
                databaseLink.Close();
                return databaseLink.State == System.Data.ConnectionState.Closed;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        public bool ExecuteCommand(string command, SqlParameter[] parameters)
        {
            var sqlCommand = new SqlCommand();

            sqlCommand.CommandType = System.Data.CommandType.Text;
            sqlCommand.CommandText = command;
            sqlCommand.Parameters.AddRange(parameters);

            OpenConnection();
            sqlCommand.Connection = databaseLink;

            var result = sqlCommand.ExecuteNonQuery();
            CloseConnection();

            return result > 0;
        }

        public DataSet Lecture(string command)
        {
            var info = new DataSet();

            OpenConnection();
            var adapter = new SqlDataAdapter(command, databaseLink);
            adapter.Fill(info);
            CloseConnection();

            return info;
        }

        public object ExecuteStoredProcedure(string command, SqlParameter[] parameters)
        {
            var sqlCommand = new SqlCommand();

            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = command;
            sqlCommand.Parameters.AddRange(parameters);

            OpenConnection();
            sqlCommand.Connection = databaseLink;

            var result = sqlCommand.ExecuteScalar();
            CloseConnection();

            return result;
        }
    }
}
