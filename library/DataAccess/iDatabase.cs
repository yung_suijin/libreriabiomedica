﻿using System;
using System.Collections.Generic;
using System.Text;

namespace library.DataAccess
{
    public interface iDatabase<T> where T : class
    {
        bool Insert(T element);

        bool Update(T element);

        bool Delete(T element);

        List<T> Select();
    }
}
