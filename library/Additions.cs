﻿using System;
using System.Collections.Generic;
using System.Text;

namespace library
{
    public class Additions
    {
        public void ClearScreenAfterKey()
        {
            Console.WriteLine("\nPress enter to get back to Administrator Menu.");
            Console.ReadKey();
            Console.Clear();
        }

        public string HideKeyboardPassword()
        {
            var text = string.Empty;

            while(true)
            {
                ConsoleKeyInfo key = Console.ReadKey(true);

                if (key.Key == ConsoleKey.Enter)
                    break;
                else if(key.Key == ConsoleKey.Backspace)
                {
                    if(text.Length > 0)
                    {
                        text = text.Substring(0, (text.Length - 1));
                        Console.Write("\b \b");
                    }
                }
                else if(key.KeyChar != '\u0000')
                {
                    text += key.KeyChar;
                    Console.Write("*");
                }
            }

            return text;
        }
    }
}
