﻿using System;
using System.Collections.Generic;
using System.Text;

namespace library
{
    public class Validations
    {
        public int ValidateIntegerPositiveValue(string message)
        {
            int value;
            string number;

            do
            {
                Console.Write(message);
                number = Console.ReadLine();

                Int32.TryParse(number, out value);
            }
            while (value == 0);

            return value;
        }

        public double ValidateDoublePositiveValue(string message)
        {
            double value;
            string number;

            do
            {
                Console.Write(message);
                number = Console.ReadLine();

                Double.TryParse(number, out value);
            }
            while (value == 0);

            return value;
        }
    }
}
