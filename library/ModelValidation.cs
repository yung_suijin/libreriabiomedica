﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace library
{
    public class ModelValidation<T> where T : class
    {
        public List<ValidationResult> ValidateObject(T element)
        {
            var context = new ValidationContext(element);
            var errors = new List<ValidationResult>();

            Validator.TryValidateObject(element, context, errors, true);
            return errors;
        }
    }
}
